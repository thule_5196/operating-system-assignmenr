

PATH=dist/build

demo : demo1 demo2 demo3
demo1 : $(TARGET)
	(usr/bin/perl ca1_jobs.pl 10 2 15) | ./$(PATH)/$(TARGET)/$(TARGET) | tee demo_output1.txt
demo2 : $(TARGET)
	(perl ca1_jobs.pl 20 15 30) | ./$(PATH)/$(TARGET)/$(TARGET) | tee demo_output2.txt
demo3 : $(TARGET)
	(perl ca1_jobs.pl 5 1 20) | ./$(PATH)/$(TARGET)/$(TARGET) | tee demo_output3.txt

.PHONY: cleanup
cleanup :
