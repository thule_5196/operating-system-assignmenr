import Control.Monad
import System.IO
import Data.List
import Schedule
import System.Environment

main = do  
 -- readFile <- getArgs
 handle <- openFile "./test.txt" ReadWriteMode
 -- let contents= head readFile
 contents<-hGetContents handle
 let xs=(map words (lines contents))
 let arriveCheck = map (\ys -> (head (tail ys)) : [(head ys)]) xs
 let input=(map convert xs)
 let fifo_out=(fifo_wrap (input))
 let stcf_out=(stcf_wrap (input))
 let sjf_out=(sjf_wrap (input))
 let rr_out=(rr_wrap (input))
 let rr5_out=(rr_wrap5 (input))
 let temp = [fifo_out,sjf_out,stcf_out,rr_out,rr5_out]
 let result = (map show [0..length fifo_out]) :temp
 putStrLn "? *COMPLETE is also the last step of the job "
 putStrLn "#\tFIFO\tSJF\tSTCF\tRR1\tRR2"
 mapM (printList arriveCheck) (tail (transpose result)) 
 putStrLn "= SIMULATION COMPLETE" 
 let result = (replicate (length ((map convert xs))) "R"): transpose (((response_time_wrap_vertical (input) (temp))))
 putStrLn "#\tJOB\tFIFO\tSJF\tSTCF\tRR1\tRR2"
 mapM (printList []) (transpose result)
 let result = (replicate (length ((map convert xs))) "T"): transpose (((turn_around_time_vertical (input) (temp))))
 mapM (printList []) (transpose result) 
 putStrLn "= INDIVIDUAL STATS COMPLETE" 
 putStrLn "# SCHEDULER\tAVG_TURNAROUND\tAVG_RESPONSE"
 let avgs_r= get_average_response_time_wrap (input) (temp)
 let avgs_t= get_average_turn_around_time_wrap (input) (temp)
 printAvgs avgs_t avgs_r
 putStrLn "= AGGREGATE  STATS COMPLETE" 

------------- print (helpers)
 
printAvgs:: [Double]->[Double]->IO()
printAvgs a b = do
 putStrLn ("@FIFO\t\t" ++ (show (a!!0)) ++"\t\t" ++(show (b!!0)))
 putStrLn ("@SJF\t\t" ++ (show (a!!1)) ++"\t\t" ++(show (b!!1)))
 putStrLn ("@STCF\t\t" ++ (show (a!!2)) ++"\t\t" ++(show (b!!2)))
 putStrLn ("@RR1\t\t" ++ (show (a!!3)) ++"\t\t" ++(show (b!!3)))
 putStrLn ("@RR5\t\t" ++ (show (a!!4)) ++"\t\t" ++(show (b!!4)))
 

-- Edited From: https://stackoverflow.com/questions/36502375/haskell-how-to-print-each-element-of-list-separated-with-comma
-- in my opinion, the arrival time should be print here rather than within the simulation because, no matter what algorithm is use, the arrival time will not be changed
printList :: [[String]]->[String]-> IO ()
printList ys [] = return()
printList ys [x]= putStrLn x
printList ys (x:xs)= do
  let k=filter (\a -> head a ==x) ys
  if (length (k)  >0) then do
   putStrLn ("* ARRIVE "++ ((head (tail (head k)))))
   putStr x 
   putStr "\t"
   printList ys xs
  else do
   putStr x
   putStr "\t"
   printList ys xs