import Control.Monad
import System.IO
import Data.List
import Schedule2

main = do
 let a = "a"
 handle <- openFile "./test.txt" ReadWriteMode
 contents<-hGetContents handle
 let xs=(map words (lines contents))
 let input=(map convert xs)
 process_wrap input
