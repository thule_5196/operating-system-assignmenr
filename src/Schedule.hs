module Schedule(
 convert,
 quick_sort_duration,
 fifo_wrap,
 my_fold,
 my_zip,
 quick_sort_arrival,
 Job,
 stcf_wrap,
 remove,
 sjf_wrap,
 rr_wrap,
 rr_wrap5,
 find_first,
 response_time_wrap_vertical,
 turn_around_time_vertical,
 get_average_response_time_wrap,
 get_average_turn_around_time_wrap
) where
import Data.List

-- data type declaration ----
data Job = Job String Int Int deriving (Show)

jobName :: Job -> String
jobName
 (Job name _ _) = name

arrivalTime :: Job -> Int
arrivalTime
 (Job _ arrive _) = arrive
 
duration :: Job -> Int
duration
 (Job _ _ dur) = dur
 
-- Simulation----- 

---- Ultils-------------------


---- convert from list of String to Job
convert :: [String] -> Job
convert xs =
 Job (head xs) (read (head (tail xs)) :: Int) (read (last xs) :: Int)

---- find min duration from a list of jobs

min_duration :: [Job] -> Job 
min_duration [] = Job " " 0 1
min_duration [x] = x  
min_duration (x:y:xs) = if duration x < duration y then min_duration(x:xs) else min_duration(y:xs)
 
------- remove a job from a list of Job
remove _ [] = []
remove x (y:ys) 
 | (jobName x == jobName y) && (duration x == duration y) && (duration x == duration y) = remove x ys    
 | otherwise = y : remove x ys

------ update a job from a list of job, => i.e: decrease the remaining duration by 1
update _ [] = []
update x (y:ys) 
 | (jobName x == jobName y) && (duration x == duration y) && (duration x == duration y) && (duration y >1) = (Job (jobName y) (arrivalTime y) ((duration y) -1)) : update x ys
 | (jobName x == jobName y) && (duration x == duration y) && (duration x == duration y) && (duration y == 1) = update x ys
 | otherwise = y : update x ys

------decrease the reamining duration of a job by 1
decrease:: Job -> Job
decrease (Job a b x) = (Job a b (x-1)) 

-- decrease the first one of a list
chop1 (x:xs) = ((decrease x): xs)

------ sort by arrivalTime - ascending
quick_sort_arrival :: [Job] -> [Job]
quick_sort_arrival []     = []
quick_sort_arrival (p:xs) = (quick_sort_arrival lesser) ++ [p] ++ (quick_sort_arrival greater)
 where
  lesser  = filter (\ x -> (arrivalTime x) < (arrivalTime p)) xs
  greater = filter (\ x -> (arrivalTime x) >= (arrivalTime p)) xs

----sort by duration
quick_sort_duration :: [Job] -> [Job]
quick_sort_duration []     = []
quick_sort_duration (p:xs) = (quick_sort_duration lesser) ++ [p] ++ (quick_sort_duration greater)
 where
  lesser  = filter (\ x -> (duration x) < (duration p)) xs
  greater = filter (\ x -> (duration x) >= (duration p)) xs

---- grouping all jobs with the same arriveal time together
my_fold:: [Job] -> [Job]-> [[Job]]
my_fold [] ys = [ys]
my_fold (x:xs) ys
 | length ys == 0 = my_fold xs [x]
 | (arrivalTime x) == (arrivalTime (head ys)) = my_fold xs (x:ys) 
 | otherwise = (ys : (my_fold xs [x]))
 
----- give it a tag which is the arrival Time 
my_zip:: [[Job]] -> [(Int, [Job])]
my_zip xs = map (\ x -> (arrivalTime (head x), x)) xs
 
  
------------------------------------------------------------------
----------------------------- Algorithm --------------------------
------------------------------------------------------------------

---fifo algorithm ---------------------------  
fifo :: [Job] -> Int -> [String]
fifo [] _ = []
fifo (x:xs) n
 | n < (arrivalTime x) = [""] ++ (fifo (x:xs) (n+1))
 |(n >= (arrivalTime x)) && (duration x >0) = if (duration x >1) then [jobName x] ++ (fifo ((Job (jobName x) (arrivalTime x) ((duration x)-1)):xs) (n+1)) else ["* COMPLETE " ++jobName x] ++ (fifo ((Job (jobName x) (arrivalTime x) ((duration x)-1)):xs) (n+1))
 |(n >= (arrivalTime x)) && (duration x ==0) = ([] ++ (fifo xs (n+1)) )
 
fifo_wrap :: [Job] -> [String]
fifo_wrap xs = fifo (quick_sort_arrival xs) 0
 
----sjf------------------ 

sjf::[(Int,[Job])] -> Int -> [Job] -> Job-> [String]
sjf [] n [] (Job _ _ 0) =[]
sjf [] n [] a = [jobName a] ++ (sjf [] (n+1) [] (decrease a))

sjf [] n ys a
 |duration a >0 = [s ++ jobName a]++ (sjf [] (n+1) ys (decrease a) )
 |duration a==0 = [jobName(min_duration ys)] ++ (sjf [] (n+1) (remove (min_duration ys) ys) (decrease(min_duration ys)))
 where 
  s = if duration a ==1
         then "* COMPLETE"
         else ""
		 
sjf (x:xs) n ys a
 |n < (fst x) && duration a >0 = [s++jobName(a)] ++ (sjf (x:xs) (n+1) (ys) (decrease a))
 |n < (fst x) && duration a ==0 = [jobName(min_duration ys)] ++ (sjf (x:xs)  (n+1) (remove (min_duration ys) ys) (decrease(min_duration ys)))
 |n >= (fst x) && duration a >0 = [s++jobName(a)] ++ (sjf (xs)  (n+1) (zs) (decrease a))
 |n >= (fst x) && duration a ==0 = [jobName(min_duration zs)] ++ (sjf (xs)  (n+1) (remove (min_duration zs) zs) (decrease(min_duration zs))) 
 where 
  zs = (snd x)++ ys
  s = if duration a ==1
         then "* COMPLETE"
         else ""

sjf_wrap :: [Job] -> [String]
sjf_wrap xs = sjf ((my_zip(my_fold (quick_sort_arrival(xs)) []))) 0 [] (Job "" 0 1)
 


----stcf----------------

stcf::[(Int,[Job])] -> Int -> [Job] -> [String]
stcf [] _ []= []
stcf [] n ys = [s++jobName(min_duration ys)] ++ (stcf [] (n+1) (update (min_duration ys) ys))
 where 
 s = if ((duration (min_duration ys))==1)
         then "* COMPLETE"
         else ""
stcf (x:xs) n ys
 |n < (fst x) = [s++jobName(min_duration ys)] ++ (stcf (x:xs)  (n+1) (update (min_duration ys) ys))
 |otherwise = [s++jobName(min_duration zs)] ++ (stcf (xs)  (n+1) (update (min_duration zs) zs)) 
 where 
 zs = (snd x)++ ys
 s = if ((n < (fst x)) && (duration (min_duration ys) ==1) || ((n >= (fst x)) && (duration(min_duration zs) ==1)))
         then "* COMPLETE"
         else ""

stcf_wrap :: [Job] -> [String]
stcf_wrap xs = stcf ((my_zip(my_fold (quick_sort_arrival(xs)) []))) 0 []



------- rr :: slice -> Q -> tick 
rr:: (Int,Int) -> [(Int,[Job])] -> Int -> [Job] -> [String]
rr (i,n) [] _ []= []

rr (i,j) (x:xs) n []
 | n < (fst x) && i<j = [] ++ (rr (i+1,j) (x:xs) (n+1) [])
 | n < (fst x) && i>=j = [] ++ (rr (0,j) (x:xs) (n) [])
 | n >= (fst x) = [] ++ (rr (i,j) (xs) (n) (snd x))

rr (i,j) [] n (y:ys) 
 | duration y >0 && i< j= [s++jobName(y)] ++ (rr (i+1,j) []  (n+1) ([decrease y]++ys))
 | duration y >0 && i>= j= [] ++ (rr (0,j) []  (n) (ys++ [y]))
 | duration y ==0 = [] ++ (rr (0,j) []  (n) (ys))
 where 
 s = if ((duration (y))==1)
         then "* COMPLETE"
         else ""
		 
rr (i,j) (x:xs) n (y:ys) 
 |n < (fst x) && duration y>0 && i<j= [s++jobName(y)] ++ (rr (i+1,j) (x:xs)  (n+1) ([decrease y]++ys))
 |n < (fst x) && duration y>0 && i>=j = [] ++ (rr (0,j) (x:xs)  (n) (ys++ [y]))
 |n < (fst x) && duration y==0 = [] ++ (rr (0,j) (xs)  (n) (ys))
 |n >= (fst x) && duration y>0 && i<j = [s++jobName(y)] ++ (rr (i+1,j) (xs)  (n+1) ([decrease y] ++ ys ++ (snd x)))
 |n >= (fst x) && duration y>0 && i>=j= [] ++ (rr (0,j) (xs)  (n) (ys++ (snd x) ++ [y]))
 |n >= (fst x) && duration y==0 = [] ++ (rr (0,j) (xs)  (n) (ys++ (snd x)))
 where 
 s = if ((duration (y))==1)
         then "* COMPLETE"
         else ""
----rr1- time slice 1-------------

rr_wrap :: [Job] -> [String]
rr_wrap xs = rr (0,1) ((my_zip(my_fold (quick_sort_arrival(xs)) []))) 0 [Job " " 0 1]

--rr2---time slice 5------------
rr_wrap5 :: [Job] -> [String]
rr_wrap5 xs = rr (0,5) ((my_zip(my_fold (quick_sort_arrival(xs)) []))) 0 [Job " " 0 1]


--- per job stats----------

----- find the first time the job was run
find_first:: Job -> [String] -> Int
find_first (Job name arr dur) [] = (-1)
find_first (Job name arr dur) (x:xs)
 |x /= name = 1 + find_first (Job name arr dur) (xs)
 |otherwise = 0
 
-----response_time-------

-------calculate single response_time
response_time:: Int->Int -> Int
response_time first_run arrive =first_run - arrive

-------calculate response_time for all algorithm of the same job
response_time_wrap:: Job -> [String]->Int
response_time_wrap x ys = response_time (find_first x ys) ( arrivalTime x)

-------calculate response_time for all jobs and all algorithm

response_time_wrap_vertical:: [Job] ->[[String]]->[[String]]
response_time_wrap_vertical [] (ys) = [] 
response_time_wrap_vertical (x:xs) (ys) = [[(jobName x)] ++ (map show (map (response_time_wrap x) ys))] ++ (response_time_wrap_vertical (xs) (ys))

-----turn around time-----

-------find finsihing point of the job by looking up from the bottom
find_last:: Job -> [String] -> Int
find_last x ys = (length ys) - (find_first x (reverse ys))
-------calculate single turn_around_time

turn_around_time:: Int->Int->Int
turn_around_time last_complete arrive = last_complete - arrive
-------calculate response_time for all algorithm of the same job

turn_around_time_wrap:: Job -> [String]->Int
turn_around_time_wrap x ys = turn_around_time (find_last x ys) ( arrivalTime x)
-------calculate response_time for all jobs and all algorithm

turn_around_time_vertical:: [Job] ->[[String]]->[[String]]
turn_around_time_vertical [] (ys) = [] 
turn_around_time_vertical (x:xs) (ys) = [[(jobName x)] ++ (map show (map (turn_around_time_wrap x) ys))] ++ (turn_around_time_vertical (xs) (ys)) 
 
---- average stats -----

-------calculate response_time for all jobs and all algorithm, similar to response_time_wrap_vertical but return a matrix of String not the pretty list of String to print
response_time_wrap_vertical_calc:: [Job] ->[[String]]->[[Int]]
response_time_wrap_vertical_calc [] (ys) = [] 
response_time_wrap_vertical_calc (x:xs) (ys) = [(map (response_time_wrap x) ys)] ++ (response_time_wrap_vertical_calc (xs) (ys))

-------calculate turn_around_time for all jobs and all algorithm, similar to turn_around_time_vertical but return a matrix of String not the pretty list of String to print

turn_around_time_wrap_vertical_calc:: [Job] ->[[String]]->[[Int]]
turn_around_time_wrap_vertical_calc [] (ys) = [] 
turn_around_time_wrap_vertical_calc (x:xs) (ys) = [(map (turn_around_time_wrap x) ys)] ++ (turn_around_time_wrap_vertical_calc (xs) (ys))

------ transpose the array so it is now indexed against algorithms and get sum of each
get_sum_time:: [[Int]] -> [Int]
get_sum_time [] = []
get_sum_time (xs) = map (sum) (transpose xs)

------ get an array of result, for each metric
get_average_response_time_wrap xs ys = map (\x -> (fromIntegral  x)/(fromIntegral (length xs))) (get_sum_time (response_time_wrap_vertical_calc xs ys))
get_average_turn_around_time_wrap xs ys = map (\x -> (fromIntegral  x)/(fromIntegral (length xs))) (get_sum_time (turn_around_time_wrap_vertical_calc xs ys))
 
 
 
 
 
 
 
 
 
 
 
 
 
 

