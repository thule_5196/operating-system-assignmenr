module Schedule2(
blah,
Sim,
State,
Job,
fifo,
convert,
quick_sort_arrival,
arrived,
process_wrap
) where 

blah :: String -> String
blah a = a

--------------utils-----------
convert :: [String] -> Job
convert xs =
 Job (head xs) (read (head (tail xs)) :: Int) (read (last xs) :: Int)

quick_sort_arrival :: [Job] -> [Job]
quick_sort_arrival []     = []
quick_sort_arrival (p:xs) = (quick_sort_arrival lesser) ++ [p] ++ (quick_sort_arrival greater)
 where
  lesser  = filter (\ x -> (arrivalTime x) < (arrivalTime p)) xs
  greater = filter (\ x -> (arrivalTime x) >= (arrivalTime p)) xs

  
arrived :: [Job] -> Int -> [Job]
arrived [] _ = []
arrived (job:jobs) i
 | (arrivalTime (job) == i) =  [job]++(arrived jobs i)
 | otherwise = [] ++ (arrived jobs i)


-- data type declaration ----

--Job-------
data Job = Job String Int Int deriving (Show)

jobName :: Job -> String
jobName
 (Job name _ _) = name

arrivalTime :: Job -> Int
arrivalTime
 (Job _ arrive _) = arrive
 
duration :: Job -> Int
duration
 (Job _ _ dur) = dur
 
--State---------

data State = State String Int Bool deriving (Show)

currentJob :: State -> String
currentJob (State name _ _) = name

remaining :: State -> Int
remaining (State _ rem _) = rem

isLock :: State -> Bool
isLock (State _ _ l) = l

-- Sim------------
data Sim = Sim State [Job] Int deriving (Show)

currentState :: Sim -> State
currentState (Sim s _ _) = s

arrivedJobs :: Sim -> [Job]
arrivedJobs (Sim _ xs _) = xs

currentTick :: Sim -> Int
currentTick (Sim _ _ t) = t
 
fifo :: Sim -> Sim
fifo (Sim (State _ 0 a) [] tick) = (Sim (State "Waiting" 0 a) [] (tick+1))
fifo (Sim (State x y t) jobs tick)
 | y > 0 = (Sim (State x (y-1) t) jobs (tick+1))
 | otherwise = (Sim (State (jobName a) ((duration a) -1 ) t) (tail jobs) (tick+1))
 where a = head jobs

mytail [] = []
mytail xs = tail xs
 
process_wrap input = process 0 input [] (Sim (State "starting" 0 True) [] 0)
 
process 1000 _ _ _= return ()
process n jobs cur st=
 do
  let a = cur ++ (arrived jobs n)
  let b = (fifo (Sim (currentState st) a n))
  if (remaining (currentState b) == 0) then do
   putStrLn (show (fifo (Sim (currentState st) a n)))
   process (n+1) jobs (mytail a) b
  else do 
   putStrLn (show (fifo (Sim (currentState st) a n)))
   process (n+1) jobs a b